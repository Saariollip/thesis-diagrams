
# Create the input vectors.
techs <- c("Mobiilisovellus", "Web mobiilissa")
colors <- c(gray(0.45), gray(0.9))


# Create the matrix of the values. 4 x 3, from bottom left to bottom right then
values <- matrix(c(4.0, 11.4))


# Give the chart file a name.
png(file = "output/avg_monthly_audience.png", width = 800, height = 800)

# Create the bar chart.
barplot( space= 0.4, values, main = "Keskimääräinen kävijämäärä kuukaudessa",names.arg = techs , xlab = "Mitattu kesäkuussa 2016", ylab = "Kävijät miljoonittain",
   col = colors, beside=TRUE)


text (0.9, 3.5, values[1], cex=2)
text (2.3, 10.9, values[2], cex=2)

# Save the file.
dev.off()








#Data source: http://image.slidesharecdn.com/2016usmobileappreportslidesharefinal-160913131130/95/comscore-2016-us-mobile-app-report-8-638.jpg?cb=1473772419