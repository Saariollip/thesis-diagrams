# Create the input vectors.


browsers <- c("Chrome","Firefox","IE","Safari", "Opera)
colors <- c(gray(0.3), gray(0.6), gray(0.9))
regions <- c("Työpöytä (web)","Mobiilisovellus","Web mobiilissa")

# Create the matrix of the values. 4 x 3, from bottom left to bottom right then
values <- matrix(c(476553, 480967, 550522, 491743, 409847, 621410, 778954, 864328, 77081, 97440, 118299, 124787),nrow = 3,ncol = 4,byrow = TRUE)

'''
group	version	value
Chrome	C54	62.9
Chrome	C53	3.5
Chrome	Vanhemmat kuin C47	2.9
Chrome	Muut	4.8
Firefox	FF49	7.1
Firefox	FF50	3.5
Firefox	Vanhemmat kuin FF43	1
Firefox	Muut	3.2
IE	IE11	3.2
IE	Edge14	1
IE	Vanhemmat kuin IE10	0.5
IE	Muut	0.5
Safari	S10	2.3
Safari	S9	0.8
Safari	Vanhemmat kuin S6	0.2
Safari	Muut	0.2
Opera	O41	0.5
Opera	Omini	0.3
Opera	Vanhemmat kuin O35	0.3
Opera	Muut	0
'''

# Give the chart file a name.
png(file = "output/digital_media_time_spent.png", width = 800, height = 800)

# Create the bar chart.
barplot( space= 0.4, values, main = "Digitaalinen ajankayttö",names.arg = months,xlab = "Vuodet (mitattu kesäkuussa)",ylab = "Minuutit miljoonittain",
   col = colors)

#Printing values to the bars
iters <- 4 #Amount of cols
m <- 0.4 #Space multiplier

for (i in 1:iters){
	text(i-0.5+i*m, 400000, values[1, i], cex=1.2)
	text(i-0.5+i*m, 800000, values[2, i], cex=1.2)
	if(i==1){
		text(i-0.5+i*m, 930000, values[3, i], cex=1.2)
	}
	else if(i==2){
		text(i-0.5+i*m, 1150000, values[3, i], cex=1.2)
	}
	else if(i==3){
		text(i-0.5+i*m, 1400000, values[3, i], cex=1.2)
	}
	else{
		text(i-0.5+i*m, 1440000, values[3,1], cex=1.2)
	}
}

#Printing percent change between the bars
iter <- 3
m <- 0.4 #Space between text
for (i in 1:iter){

	if ((round((values[1,1+i]/values[1,0+i]*100), digits = 0 )) - 100 > 0 )
	{
		percentChange <- paste("+", toString((round((values[1,1+i]/values[1,0+i]*100), digits = 0 ))-100), "%", sep="")
	}	
	else{
		percentChange <- paste(toString((round((values[1,1+i]/values[1,0+i]*100), digits = 0 ))-100), "%", sep="")
	}
		text(i+0.215+i*m, 460000, percentChange, cex=1.2)
	
	if ((round((values[2,1+i]/values[2,0+i]*100), digits = 0 )) - 100 > 0 )
	{
		percentChange <- paste("+", toString((round((values[2,1+i]/values[2,0+i]*100), digits = 0 ))-100), "%", sep="")
	}	
	else{
		percentChange <- paste(toString((round((values[2,1+i]/values[2,0+i]*100), digits = 0 ))-100), "%", sep="")
	}
		text(i+0.215+i*m, 860000, percentChange, cex=1.2)
	
	if ((round((values[3,1+i]/values[3,0+i]*100), digits = 0 )) - 100 > 0 )
	{
		percentChange <- paste("+", toString((round((values[3,1+i]/values[3,0+i]*100), digits = 0 ))-100), "%", sep="")
	}	
	else{
		percentChange <- paste(toString((round((values[3,1+i]/values[3,0+i]*100), digits = 0 ))-100), "%", sep="")
	}
		text(i+0.21+i*m, 960000+i*150000, percentChange, cex=1.2)
	#Overall percent Change
	
}
#Print overall percent change
overallPercentChangeDesktop <- paste("Muutosprosentti\nkesäkuusta: +", toString(round((values[1,4]/values[1,1])*100-100)), "%", sep="")
text(5.1, 340000, overallPercentChangeDesktop)
overallPercentChangeMobile <- paste("Muutosprosentti\nkesäkuusta: +", toString(round((values[2,4]/values[2,1])*100-100)), "%", sep="")
text(5.1, 740000, overallPercentChangeMobile)
overallPercentChangeMobileWeb <- paste("Muutosprosentti\nkesäkuusta: +", toString(round((values[3,4]/values[3,1])*100-100)), "%", sep="")
text(5.1, 1395000, overallPercentChangeMobileWeb)

# Add the legend to the chart.
legend("topleft", regions, cex = 1.3, fill = colors)

# Save the file.
dev.off()

#Data source https://arc.applause.com/2016/09/13/native-apps-versus-mobile-web-decision/ Growth in Digital Media Time Spent
